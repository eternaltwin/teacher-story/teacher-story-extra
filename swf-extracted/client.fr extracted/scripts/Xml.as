package
{
   import §vaW\x1e\x01§.§ohb+§;
   
   public class Xml
   {
      
      public static var §\x1bf\x15]\x02§:String = "element";
      
      public static var §\tZ\f\x1c§:String = "pcdata";
      
      public static var §@L\x1f*\x03§:String = "cdata";
      
      public static var §i\x01Zn§:String = "comment";
      
      public static var §S\x02RM\x02§:String = "doctype";
      
      public static var §AJY?\x03§:String = "prolog";
      
      public static var $6HZ:String = "document";
       
      
      public var §\bF;~\x03§:Xml;
      
      public var §8,\x07j§:String;
      
      public var _node:XML;
      
      public function Xml()
      {
      }
      
      public static function §Cs\x1b\x03§(param1:String) : Xml
      {
         var _loc5_:* = null as TypeError;
         var _loc6_:* = null as §M\x07N]§;
         var _loc7_:* = null as String;
         XML.ignoreWhitespace = false;
         XML.ignoreProcessingInstructions = false;
         XML.ignoreComments = false;
         var _loc4_:XML = null;
         while(_loc4_ == null)
         {
            try
            {
               _loc4_ = new XML("<__document" + ">" + param1 + "</__document>");
            }
            catch(:TypeError)
            {
               _loc5_ = ;
               if(_loc5_ as Error)
               {
                  §ohb+§.§;\x13=\x04§ = _loc5_;
               }
               _loc6_ = new §M\x07N]§("\"([^\"]+)\"","");
               if(_loc5_.errorID == 1083 && Boolean(_loc6_.§\x11O`\x13\x02§(_loc5_.message)))
               {
                  _loc7_ = _loc6_.§v\x1c\x1eB\x02§(1);
                  var _loc3_:String = "<__document" + (" xmlns:" + _loc7_ + "=\"@" + _loc7_ + "\"");
                  continue;
               }
               §ohb+§.§;\x13=\x04§ = new Error();
               throw _loc5_;
            }
         }
         return Xml.§QES+§(_loc4_,Xml.$6HZ);
      }
      
      public static function §u\ff\x0e\x02§(param1:Xml, param2:Xml) : Boolean
      {
         return param1 == null?param2 == null:param2 == null?false:param1._node == param2._node;
      }
      
      public static function §P\x1cF(§(param1:String) : Xml
      {
         return Xml.§QES+§(new XML("<" + param1 + "/>"),Xml.§\x1bf\x15]\x02§);
      }
      
      public static function §(\x1b$S\x02§(param1:String) : Xml
      {
         XML.ignoreWhitespace = false;
         return Xml.§QES+§(new XML(param1),Xml.§\tZ\f\x1c§);
      }
      
      public static function §d^}\x1b\x01§(param1:String) : Xml
      {
         return Xml.§QES+§(new XML("<![CDATA[" + param1 + "]]>"),Xml.§@L\x1f*\x03§);
      }
      
      public static function §,\x06%=§(param1:String) : Xml
      {
         XML.ignoreComments = false;
         return Xml.§QES+§(new XML("<!--" + param1 + "-->"),Xml.§i\x01Zn§);
      }
      
      public static function §5u,n\x01§(param1:String) : Xml
      {
         return Xml.§QES+§(new XML("<!DOCTYPE " + param1 + ">"),Xml.§S\x02RM\x02§);
      }
      
      public static function §\x0efqo\x03§(param1:String) : Xml
      {
         XML.ignoreProcessingInstructions = false;
         return Xml.§QES+§(new XML("<?" + param1 + "?>"),Xml.§AJY?\x03§);
      }
      
      public static function §ofw=\x03§() : Xml
      {
         return Xml.§QES+§(<__document/>,Xml.$6HZ);
      }
      
      public static function §J;uV\x02§(param1:XML) : String
      {
         var _loc2_:String = param1.nodeKind();
         if(_loc2_ == "element")
         {
            return Xml.§\x1bf\x15]\x02§;
         }
         if(_loc2_ == "text")
         {
            return Xml.§\tZ\f\x1c§;
         }
         if(_loc2_ == "processing-instruction")
         {
            return Xml.§AJY?\x03§;
         }
         if(_loc2_ == "comment")
         {
            return Xml.§i\x01Zn§;
         }
         §ohb+§.§;\x13=\x04§ = new Error();
         throw "unimplemented node type: " + §5\f\x1d§.§%^/y§(param1.nodeType);
      }
      
      public static function §QES+§(param1:XML, param2:String = undefined) : Xml
      {
         var _loc3_:Xml = new Xml();
         _loc3_._node = param1;
         _loc3_.§8,\x07j§ = param2 != null?param2:Xml.§J;uV\x02§(param1);
         return _loc3_;
      }
      
      public function §QtJ\x0b\x03§(param1:XMLList) : Array
      {
         var _loc5_:int = 0;
         var _loc2_:Array = [];
         var _loc3_:int = 0;
         var _loc4_:int = int(param1.length());
         while(_loc3_ < _loc4_)
         {
            _loc3_++;
            _loc5_ = _loc3_;
            _loc2_.push(Xml.§QES+§(param1[_loc5_]));
         }
         return _loc2_;
      }
      
      public function toString() : String
      {
         var _loc1_:* = null as String;
         XML.prettyPrinting = false;
         if(§8,\x07j§ == Xml.$6HZ)
         {
            _loc1_ = _node.toXMLString();
            _loc1_ = _loc1_.substr(int(_loc1_.indexOf(">")) + 1);
            _loc1_ = _loc1_.substr(0,_loc1_.length - 13);
            return _loc1_;
         }
         return _node.toXMLString();
      }
      
      public function _3Ui(param1:String) : String
      {
         var _loc5_:int = 0;
         var _loc6_:* = null as XMLList;
         var _loc2_:String = §8,\x07j§;
         var _loc3_:Xml = null;
         if(_loc2_ == Xml.§\x1bf\x15]\x02§ || _loc2_ == Xml.$6HZ)
         {
            §ohb+§.§;\x13=\x04§ = new Error();
            throw "bad nodeType";
         }
         if(_loc2_ == Xml.§\tZ\f\x1c§)
         {
            _loc3_ = Xml.§(\x1b$S\x02§(param1);
         }
         else if(_loc2_ == Xml.§@L\x1f*\x03§)
         {
            _loc3_ = Xml.§d^}\x1b\x01§(param1);
         }
         else if(_loc2_ == Xml.§i\x01Zn§)
         {
            _loc3_ = Xml.§,\x06%=§(param1);
         }
         else if(_loc2_ == Xml.§S\x02RM\x02§)
         {
            _loc3_ = Xml.§5u,n\x01§(param1);
         }
         else
         {
            _loc3_ = Xml.§\x0efqo\x03§(param1);
         }
         var _loc4_:XML = _node.parent();
         if(_loc4_ != null)
         {
            _loc4_.insertChildAfter(_node,_loc3_._node);
            _loc5_ = int(_node.childIndex());
            _loc6_ = _loc4_.children();
            delete _loc6_[§Hzd/§.§)~Iz\x02§(_loc6_)[_loc5_]];
         }
         _node = _loc3_._node;
         return param1;
      }
      
      public function §\x06\x11^b§(param1:String) : String
      {
         if(§8,\x07j§ != Xml.§\x1bf\x15]\x02§)
         {
            §ohb+§.§;\x13=\x04§ = new Error();
            throw "bad nodeType";
         }
         var _loc2_:Array = param1.split(":");
         if(int(_loc2_.length) == 1)
         {
            _node.setLocalName(param1);
         }
         else
         {
            _node.setLocalName(_loc2_[1]);
            _node.setNamespace(_node.namespace(_loc2_[0]));
         }
         return param1;
      }
      
      public function §Y\x12\x14§(param1:String, param2:String) : void
      {
         var _loc4_:* = null as Namespace;
         var _loc5_:* = null as XMLList;
         if(§8,\x07j§ != Xml.§\x1bf\x15]\x02§)
         {
            §ohb+§.§;\x13=\x04§ = new Error();
            throw "bad nodeType";
         }
         var _loc3_:Array = param1.split(":");
         if(_loc3_[0] == "xmlns")
         {
            _loc4_ = _node.namespace(_loc3_[1] == null?"":_loc3_[1]);
            if(_loc4_ != null)
            {
               §ohb+§.§;\x13=\x04§ = new Error();
               throw "Can\'t modify namespace";
            }
            if(_loc3_[1] == null)
            {
               §ohb+§.§;\x13=\x04§ = new Error();
               throw "Can\'t set default namespace";
            }
            _node.addNamespace(new Namespace(_loc3_[1],param2));
            return;
         }
         if(int(_loc3_.length) == 1)
         {
            _node["@" + param1] = param2;
         }
         else
         {
            _loc5_ = §\x10Y\x1d\x13\x02§(_node,_loc3_);
            _loc5_[0] = param2;
         }
      }
      
      public function §z~n%\x01§(param1:Xml) : Boolean
      {
         if(§8,\x07j§ != Xml.§\x1bf\x15]\x02§ && §8,\x07j§ != Xml.$6HZ)
         {
            §ohb+§.§;\x13=\x04§ = new Error();
            throw "bad nodeType";
         }
         var _loc2_:XMLList = _node.children();
         if(_node != param1._node.parent())
         {
            return false;
         }
         var _loc3_:int = int(param1._node.childIndex());
         delete _loc2_[§Hzd/§.§)~Iz\x02§(_loc2_)[_loc3_]];
         return true;
      }
      
      public function §Zfe\x11\x03§(param1:String) : void
      {
         if(§8,\x07j§ != Xml.§\x1bf\x15]\x02§)
         {
            §ohb+§.§;\x13=\x04§ = new Error();
            throw "bad nodeType";
         }
         var _loc2_:Array = param1.split(":");
         if(int(_loc2_.length) == 1)
         {
            §Hzd/§.§\\2~{\x01§(_node,"@" + param1);
         }
         else
         {
            delete §\x10Y\x1d\x13\x02§(_node,_loc2_)[0];
         }
      }
      
      public function §\x1dS;Z\x01§() : Object
      {
         if(§8,\x07j§ != Xml.§\x1bf\x15]\x02§ && §8,\x07j§ != Xml.$6HZ)
         {
            §ohb+§.§;\x13=\x04§ = new Error();
            throw "bad nodeType";
         }
         var _loc1_:XMLList = _node.children();
         var §(?C-\x03§:Array = §QtJ\x0b\x03§(_loc1_);
         var §ju\b§:int = 0;
         return {
            "hJMI\x02":function():Boolean
            {
               return §ju\b§ < int(§(?C-\x03§.length);
            },
            "hJRn":function():Xml
            {
               var _loc1_:int = §ju\b§;
               §ju\b§ = §ju\b§ + 1;
               return §(?C-\x03§[_loc1_];
            }
         };
      }
      
      public function §(Y\x1e}\x03§(param1:Xml, param2:int) : void
      {
         if(§8,\x07j§ != Xml.§\x1bf\x15]\x02§ && §8,\x07j§ != Xml.$6HZ)
         {
            §ohb+§.§;\x13=\x04§ = new Error();
            throw "bad nodeType";
         }
         var _loc3_:XMLList = _node.children();
         if(param2 < int(_loc3_.length()))
         {
            _node.insertChildBefore(_loc3_[param2],param1._node);
         }
         else
         {
            _node.appendChild(param1._node);
         }
      }
      
      public function §9\\]f§() : Xml
      {
         var _loc1_:XML = _node.parent();
         return _loc1_ == null?null:Xml.§QES+§(_loc1_);
      }
      
      public function §?Yq\x01§() : String
      {
         var _loc1_:String = §8,\x07j§;
         if(_loc1_ == Xml.§\x1bf\x15]\x02§ || _loc1_ == Xml.$6HZ)
         {
            §ohb+§.§;\x13=\x04§ = new Error();
            throw "bad nodeType";
         }
         if(_loc1_ == Xml.§i\x01Zn§)
         {
            return _node.toString().substr(4,-7);
         }
         return _node.toString();
      }
      
      public function §\x17\x1a/\x0f§() : String
      {
         if(§8,\x07j§ != Xml.§\x1bf\x15]\x02§)
         {
            §ohb+§.§;\x13=\x04§ = new Error();
            throw "bad nodeType";
         }
         var _loc1_:Namespace = _node.namespace();
         return _loc1_.prefix == ""?_node.localName():§5\f\x1d§.§%^/y§(_loc1_.prefix) + ":" + §5\f\x1d§.§%^/y§(_node.localName());
      }
      
      public function §\x10Y\x1d\x13\x02§(param1:XML, param2:Array) : XMLList
      {
         var _loc4_:* = null as XML;
         var _loc3_:Namespace = param1.namespace(param2[0]);
         if(_loc3_ == null)
         {
            _loc4_ = param1.parent();
            if(_loc4_ == null)
            {
               _loc3_ = new Namespace(param2[0],"@" + param2[0]);
               param1.addNamespace(_loc3_);
            }
            else
            {
               return §\x10Y\x1d\x13\x02§(_loc4_,param2);
            }
         }
         return _node.attribute(new QName(_loc3_,param2[1]));
      }
      
      public function §0oO§(param1:String) : String
      {
         var _loc3_:* = null as Namespace;
         if(§8,\x07j§ != Xml.§\x1bf\x15]\x02§)
         {
            §ohb+§.§;\x13=\x04§ = new Error();
            throw "bad nodeType";
         }
         var _loc2_:Array = param1.split(":");
         if(_loc2_[0] == "xmlns")
         {
            _loc3_ = _node.namespace(_loc2_[1] == null?"":_loc2_[1]);
            return _loc3_ == null?null:_loc3_.uri;
         }
         if(int(_loc2_.length) == 1)
         {
            if(!§Hzd/§.§z\x18;m\x03§(_node,"@" + param1))
            {
               return null;
            }
            return §Hzd/§.§\x04ue\x1c§(_node,"@" + param1);
         }
         var _loc4_:XMLList = §\x10Y\x1d\x13\x02§(_node,_loc2_);
         return int(_loc4_.length()) == 0?null:_loc4_.toString();
      }
      
      public function §0~9F\x02§() : Xml
      {
         if(§8,\x07j§ != Xml.§\x1bf\x15]\x02§ && §8,\x07j§ != Xml.$6HZ)
         {
            §ohb+§.§;\x13=\x04§ = new Error();
            throw "bad nodeType";
         }
         var _loc1_:XMLList = _node.elements();
         if(int(_loc1_.length()) == 0)
         {
            return null;
         }
         return Xml.§QES+§(_loc1_[0]);
      }
      
      public function §\x17X3y\x01§() : Xml
      {
         if(§8,\x07j§ != Xml.§\x1bf\x15]\x02§ && §8,\x07j§ != Xml.$6HZ)
         {
            §ohb+§.§;\x13=\x04§ = new Error();
            throw "bad nodeType";
         }
         var _loc1_:XMLList = _node.children();
         if(int(_loc1_.length()) == 0)
         {
            return null;
         }
         return Xml.§QES+§(_loc1_[0]);
      }
      
      public function §ch\x1bg§(param1:String) : Boolean
      {
         if(§8,\x07j§ != Xml.§\x1bf\x15]\x02§)
         {
            §ohb+§.§;\x13=\x04§ = new Error();
            throw "bad nodeType";
         }
         var _loc2_:Array = param1.split(":");
         if(_loc2_[0] == "xmlns")
         {
            return _node.namespace(_loc2_[1] == null?"":_loc2_[1]) != null;
         }
         if(int(_loc2_.length) == 1)
         {
            return Boolean(§Hzd/§.§z\x18;m\x03§(_node,"@" + param1));
         }
         return int(§\x10Y\x1d\x13\x02§(_node,_loc2_).length()) > 0;
      }
      
      public function §mwX\n\x03§(param1:String) : Object
      {
         var _loc3_:* = null as XMLList;
         var _loc4_:int = 0;
         var _loc5_:* = null as Array;
         var _loc6_:* = null as Xml;
         if(§8,\x07j§ != Xml.§\x1bf\x15]\x02§ && §8,\x07j§ != Xml.$6HZ)
         {
            §ohb+§.§;\x13=\x04§ = new Error();
            throw "bad nodeType";
         }
         var _loc2_:Array = param1.split(":");
         if(int(_loc2_.length) == 1)
         {
            _loc3_ = _node.elements(param1);
         }
         else
         {
            _loc3_ = _node.elements();
         }
         var §(?C-\x03§:Array = §QtJ\x0b\x03§(_loc3_);
         if(int(_loc2_.length) != 1)
         {
            _loc4_ = 0;
            _loc5_ = §(?C-\x03§.§7wS)§();
            while(_loc4_ < int(_loc5_.length))
            {
               _loc6_ = _loc5_[_loc4_];
               _loc4_++;
               if(_loc6_._node.localName() != _loc2_[1] || _loc6_._node.namespace().prefix != _loc2_[0])
               {
                  §(?C-\x03§.§Zfe\x11\x03§(_loc6_);
               }
            }
         }
         var §ju\b§:int = 0;
         return {
            "hJMI\x02":function():Boolean
            {
               return §ju\b§ < int(§(?C-\x03§.length);
            },
            "hJRn":function():Xml
            {
               var _loc1_:int = §ju\b§;
               §ju\b§ = §ju\b§ + 1;
               return §(?C-\x03§[_loc1_];
            }
         };
      }
      
      public function §;@O\\\x03§() : Object
      {
         if(§8,\x07j§ != Xml.§\x1bf\x15]\x02§ && §8,\x07j§ != Xml.$6HZ)
         {
            §ohb+§.§;\x13=\x04§ = new Error();
            throw "bad nodeType";
         }
         var _loc1_:XMLList = _node.elements();
         var §(?C-\x03§:Array = §QtJ\x0b\x03§(_loc1_);
         var §ju\b§:int = 0;
         return {
            "hJMI\x02":function():Boolean
            {
               return §ju\b§ < int(§(?C-\x03§.length);
            },
            "hJRn":function():Xml
            {
               var _loc1_:int = §ju\b§;
               §ju\b§ = §ju\b§ + 1;
               return §(?C-\x03§[_loc1_];
            }
         };
      }
      
      public function §?ec\x15\x02§() : Object
      {
         if(§8,\x07j§ != Xml.§\x1bf\x15]\x02§)
         {
            §ohb+§.§;\x13=\x04§ = new Error();
            throw "bad nodeType";
         }
         var §f?bC\x03§:XMLList = _node.attributes();
         var §m;m_\x02§:Array = §Hzd/§.§)~Iz\x02§(§f?bC\x03§);
         var §ju\b§:int = 0;
         return {
            "hJMI\x02":function():Boolean
            {
               return §ju\b§ < int(§m;m_\x02§.length);
            },
            "hJRn":function():Object
            {
               var _loc1_:int = §ju\b§;
               §ju\b§ = §ju\b§ + 1;
               return §f?bC\x03§[§5\f\x1d§.§R7f\x17\x03§(§m;m_\x02§[_loc1_])].name();
            }
         };
      }
      
      public function §=9;b\x01§(param1:Xml) : void
      {
         if(§8,\x07j§ != Xml.§\x1bf\x15]\x02§ && §8,\x07j§ != Xml.$6HZ)
         {
            §ohb+§.§;\x13=\x04§ = new Error();
            throw "bad nodeType";
         }
         var _loc2_:XMLList = _node.children();
         _node.appendChild(param1._node);
      }
   }
}
