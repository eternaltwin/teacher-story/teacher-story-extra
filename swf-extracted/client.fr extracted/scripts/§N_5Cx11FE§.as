package
{
   import flash.utils.describeType;
   import flash.utils.getDefinitionByName;
   import flash.utils.getQualifiedClassName;
   import flash.utils.getQualifiedSuperclassName;
   import §vaW\x1e\x01§.§ohb+§;
   
   public class §N\x11FE§
   {
       
      
      public function §N\x11FE§()
      {
      }
      
      public static function §+\x027W\x02§(param1:Object) : Class
      {
         var _loc2_:String = getQualifiedClassName(param1);
         if(_loc2_ == "null" || _loc2_ == "Object" || _loc2_ == "int" || _loc2_ == "Number" || _loc2_ == "Boolean")
         {
            return null;
         }
         if(param1.hasOwnProperty("prototype"))
         {
            return null;
         }
         var _loc3_:* = getDefinitionByName(_loc2_) as Class;
         if(_loc3_.__isenum)
         {
            return null;
         }
         return _loc3_;
      }
      
      public static function § )EM\x03§(param1:Object) : Class
      {
         var _loc2_:String = getQualifiedClassName(param1);
         if(_loc2_ == "null" || _loc2_.substr(0,8) == "builtin.")
         {
            return null;
         }
         if(param1.hasOwnProperty("prototype"))
         {
            return null;
         }
         var _loc3_:* = getDefinitionByName(_loc2_) as Class;
         if(!_loc3_.__isenum)
         {
            return null;
         }
         return _loc3_;
      }
      
      public static function §~|n/\x01§(param1:Class) : Class
      {
         var _loc2_:String = getQualifiedSuperclassName(param1);
         if(_loc2_ == null || _loc2_ == "Object")
         {
            return null;
         }
         return getDefinitionByName(_loc2_) as Class;
      }
      
      public static function §c\x0eff\x01§(param1:Class) : String
      {
         if(param1 == null)
         {
            return null;
         }
         var _loc2_:String = getQualifiedClassName(param1);
         var _loc3_:String = _loc2_;
         if(_loc3_ == "int")
         {
            return "Int";
         }
         if(_loc3_ == "Number")
         {
            return "Float";
         }
         if(_loc3_ == "Boolean")
         {
            return "Bool";
         }
         return _loc2_.split("::").join(".");
      }
      
      public static function §+n\x06\x1a\x01§(param1:Class) : String
      {
         return §N\x11FE§.§c\x0eff\x01§(param1);
      }
      
      public static function §i\x12Lx\x02§(param1:String) : Class
      {
         var _loc3_:* = null as Class;
         var _loc5_:* = null as String;
         try
         {
            _loc3_ = getDefinitionByName(param1) as Class;
            if(_loc3_.__isenum)
            {
               return null;
            }
            return _loc3_;
         }
         catch(_loc4_:*)
         {
            _loc5_ = param1;
            if(_loc5_ == "Int")
            {
               return int;
            }
            if(_loc5_ == "Float")
            {
               return Number;
            }
            return null;
         }
         if(_loc3_ == null || _loc3_.__name__ == null)
         {
            return null;
         }
         return _loc3_;
      }
      
      public static function §)wX\x1f\x01§(param1:String) : Class
      {
         var _loc3_:* = null;
         try
         {
            _loc3_ = getDefinitionByName(param1);
            if(!_loc3_.__isenum)
            {
               return null;
            }
            return _loc3_;
         }
         catch(_loc4_:*)
         {
            if(param1 == "Bool")
            {
               return Boolean;
            }
            return null;
         }
         if(_loc3_ == null || _loc3_.__ename__ == null)
         {
            return null;
         }
         return _loc3_;
      }
      
      public static function §;KP9\x01§(param1:Class, param2:Array) : Object
      {
         switch(int(param2.length))
         {
            case 0:
               §§push(new param1());
               break;
            case 1:
               §§push(new param1(param2[0]));
               break;
            case 2:
               §§push(new param1(param2[0],param2[1]));
               break;
            case 3:
               §§push(new param1(param2[0],param2[1],param2[2]));
               break;
            case 4:
               §§push(new param1(param2[0],param2[1],param2[2],param2[3]));
               break;
            case 5:
               §§push(new param1(param2[0],param2[1],param2[2],param2[3],param2[4]));
               break;
            case 6:
               §§push(new param1(param2[0],param2[1],param2[2],param2[3],param2[4],param2[5]));
               break;
            case 7:
               §§push(new param1(param2[0],param2[1],param2[2],param2[3],param2[4],param2[5],param2[6]));
               break;
            case 8:
               §§push(new param1(param2[0],param2[1],param2[2],param2[3],param2[4],param2[5],param2[6],param2[7]));
               break;
            case 9:
               §§push(new param1(param2[0],param2[1],param2[2],param2[3],param2[4],param2[5],param2[6],param2[7],param2[8]));
               break;
            case 10:
               §§push(new param1(param2[0],param2[1],param2[2],param2[3],param2[4],param2[5],param2[6],param2[7],param2[8],param2[9]));
               break;
            case 11:
               §§push(new param1(param2[0],param2[1],param2[2],param2[3],param2[4],param2[5],param2[6],param2[7],param2[8],param2[9],param2[10]));
               break;
            case 12:
               §§push(new param1(param2[0],param2[1],param2[2],param2[3],param2[4],param2[5],param2[6],param2[7],param2[8],param2[9],param2[10],param2[11]));
               break;
            case 13:
               §§push(new param1(param2[0],param2[1],param2[2],param2[3],param2[4],param2[5],param2[6],param2[7],param2[8],param2[9],param2[10],param2[11],param2[12]));
               break;
            case 14:
               §§push(new param1(param2[0],param2[1],param2[2],param2[3],param2[4],param2[5],param2[6],param2[7],param2[8],param2[9],param2[10],param2[11],param2[12],param2[13]));
         }
         return §§pop();
      }
      
      public static function §\x02](?\x03§(param1:Class) : Object
      {
         var _loc3_:* = null as Object;
         try
         {
            §ohb+§.§sjc\x04\x01§ = true;
            _loc3_ = new param1();
            §ohb+§.§sjc\x04\x01§ = false;
            return _loc3_;
         }
         catch(_loc4_:*)
         {
            §ohb+§.§sjc\x04\x01§ = false;
            §ohb+§.§;\x13=\x04§ = new Error();
            throw null;
         }
         return null;
      }
      
      public static function §j\\2`\x01§(param1:Class, param2:String, param3:Array = undefined) : Object
      {
         var _loc4_:Object = §Hzd/§.§\x04ue\x1c§(param1,param2);
         if(_loc4_ == null)
         {
            §ohb+§.§;\x13=\x04§ = new Error();
            throw "No such constructor " + param2;
         }
         if(§Hzd/§.§G_iR\x01§(_loc4_))
         {
            if(param3 == null)
            {
               §ohb+§.§;\x13=\x04§ = new Error();
               throw "Constructor " + param2 + " need parameters";
            }
            return _loc4_.apply(param1,param3);
         }
         if(param3 != null && int(param3.length) != 0)
         {
            §ohb+§.§;\x13=\x04§ = new Error();
            throw "Constructor " + param2 + " does not need parameters";
         }
         return _loc4_;
      }
      
      public static function §\x196va\x01§(param1:Class, param2:int, param3:Array = undefined) : Object
      {
         var _loc4_:String = param1.__constructs__[param2];
         if(_loc4_ == null)
         {
            §ohb+§.§;\x13=\x04§ = new Error();
            throw param2 + " is not a valid enum constructor index";
         }
         return §N\x11FE§.§j\\2`\x01§(param1,_loc4_,param3);
      }
      
      public static function §LU\x02\x0b§(param1:*, param2:Boolean) : Array
      {
         var _loc8_:int = 0;
         var _loc3_:Array = [];
         var _loc4_:XML = describeType(param1);
         if(param2)
         {
            _loc4_ = _loc4_.factory[0];
         }
         var _loc5_:XMLList = _loc4_.child("method");
         var _loc6_:int = 0;
         var _loc7_:int = int(_loc5_.length());
         while(_loc6_ < _loc7_)
         {
            _loc6_++;
            _loc8_ = _loc6_;
            _loc3_.push(§5\f\x1d§.§%^/y§(_loc5_[_loc8_].attribute("name")));
         }
         var _loc9_:XMLList = _loc4_.child("variable");
         _loc6_ = 0;
         _loc7_ = int(_loc9_.length());
         while(_loc6_ < _loc7_)
         {
            _loc6_++;
            _loc8_ = _loc6_;
            _loc3_.push(§5\f\x1d§.§%^/y§(_loc9_[_loc8_].attribute("name")));
         }
         var _loc10_:XMLList = _loc4_.child("accessor");
         _loc6_ = 0;
         _loc7_ = int(_loc10_.length());
         while(_loc6_ < _loc7_)
         {
            _loc6_++;
            _loc8_ = _loc6_;
            _loc3_.push(§5\f\x1d§.§%^/y§(_loc10_[_loc8_].attribute("name")));
         }
         return _loc3_;
      }
      
      public static function §6I\x14#\x03§(param1:Class) : Array
      {
         return §N\x11FE§.§LU\x02\x0b§(param1,true);
      }
      
      public static function §~@'r\x03§(param1:Class) : Array
      {
         var _loc2_:Array = §N\x11FE§.§LU\x02\x0b§(param1,false);
         _loc2_.§Zfe\x11\x03§("__construct__");
         _loc2_.§Zfe\x11\x03§("prototype");
         return _loc2_;
      }
      
      public static function §\x06L0\x05§(param1:Class) : Array
      {
         var _loc2_:Array = param1.__constructs__;
         return _loc2_.§7wS)§();
      }
      
      public static function §U'\x10o\x01§(param1:*) : §]{eF\x01§
      {
         var _loc5_:* = null;
         var _loc3_:String = getQualifiedClassName(param1);
         var _loc4_:String = _loc3_;
         if(_loc4_ == "null")
         {
            return §]{eF\x01§.§\t5\x13f\x03§;
         }
         if(_loc4_ == "void")
         {
            return §]{eF\x01§.§\t5\x13f\x03§;
         }
         if(_loc4_ == "int")
         {
            return §]{eF\x01§.§{u'\x05\x01§;
         }
         if(_loc4_ == "Number")
         {
            if((param1 < -268435456 || param1 >= 268435456) && int(param1) == param1)
            {
               return §]{eF\x01§.§{u'\x05\x01§;
            }
            return §]{eF\x01§.§\tl=;\x01§;
         }
         if(_loc4_ == "Boolean")
         {
            return §]{eF\x01§.§B\x1bZr\x01§;
         }
         if(_loc4_ == "Object")
         {
            return §]{eF\x01§.§\x0b'gj\x01§;
         }
         if(_loc4_ == "Function")
         {
            return §]{eF\x01§.§nF[,§;
         }
         _loc5_ = null;
         try
         {
            _loc5_ = getDefinitionByName(_loc3_);
            if(param1.hasOwnProperty("prototype"))
            {
               return §]{eF\x01§.§\x0b'gj\x01§;
            }
            if(_loc5_.__isenum)
            {
               return §]{eF\x01§.§\x18yd\x1c§(_loc5_);
            }
            return §]{eF\x01§.§7RC%\x02§(_loc5_);
         }
         catch(_loc6_:*)
         {
            if(null as Error)
            {
               §ohb+§.§;\x13=\x04§ = null;
            }
            if(_loc3_ == "builtin.as$0::MethodClosure" || int(_loc3_.indexOf("-")) != -1)
            {
               return §]{eF\x01§.§nF[,§;
            }
            return _loc5_ == null?§]{eF\x01§.§nF[,§:§]{eF\x01§.§7RC%\x02§(_loc5_);
         }
         return null;
      }
      
      public static function §mR*h\x03§(param1:Object, param2:Object) : Boolean
      {
         var _loc4_:* = null as Array;
         var _loc5_:* = null as Array;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         if(param1 == param2)
         {
            return true;
         }
         try
         {
            if(param1.§NT4h\x03§ != param2.§NT4h\x03§)
            {
               return false;
            }
            _loc4_ = param1.§zA|n\x03§;
            _loc5_ = param2.§zA|n\x03§;
            _loc6_ = 0;
            _loc7_ = int(_loc4_.length);
            while(_loc6_ < _loc7_)
            {
               _loc6_++;
               _loc8_ = _loc6_;
               if(!§N\x11FE§.§mR*h\x03§(_loc4_[_loc8_],_loc5_[_loc8_]))
               {
                  return false;
               }
            }
         }
         catch(_loc9_:*)
         {
            return false;
         }
         return true;
      }
      
      public static function §\x1e\x06)^§(param1:Object) : String
      {
         return param1.§]$x§;
      }
      
      public static function §TU`n\x02§(param1:Object) : Array
      {
         return param1.§zA|n\x03§ == null?[]:param1.§zA|n\x03§;
      }
      
      public static function §\x1e\x1e@\x0e\x01§(param1:Object) : int
      {
         return int(param1.§NT4h\x03§);
      }
      
      public static function §Z_R{§(param1:Class) : Array
      {
         var _loc5_:* = null as String;
         var _loc6_:* = null as Object;
         var _loc2_:Array = [];
         var _loc3_:Array = param1.__constructs__;
         var _loc4_:int = 0;
         while(_loc4_ < int(_loc3_.length))
         {
            _loc5_ = _loc3_[_loc4_];
            _loc4_++;
            _loc6_ = §Hzd/§.§\x04ue\x1c§(param1,_loc5_);
            if(!§Hzd/§.§G_iR\x01§(_loc6_))
            {
               _loc2_.push(_loc6_);
            }
         }
         return _loc2_;
      }
   }
}
