package
{
   import flash.Boot;
   
   public final class §j#(\x07\x02§
   {
      
      public static const __isenum:Boolean = true;
      
      public static var __constructs__ = ["_Err_ActionUnavailable","_Err_UselessAction","_Err_UselessHeal","_Err_StudentUnavailable","_Err_ObjectUnavailable","_Err_CantTargetPet","_Err_CantUseObjectHere","_Err_CantSwapEmptySeats","_Err_CantSwapUnavailableStudent","_Err_CantMoveTable","_Err_NotEnoughMoney","_Err_NoMidLife"];
      
      public static var _Err_UselessHeal:§j#(\x07\x02§;
      
      public static var _Err_UselessAction:§j#(\x07\x02§;
      
      public static var _Err_StudentUnavailable:§j#(\x07\x02§;
      
      public static var _Err_ObjectUnavailable:§j#(\x07\x02§;
      
      public static var _Err_NotEnoughMoney:§j#(\x07\x02§;
      
      public static var _Err_NoMidLife:§j#(\x07\x02§;
      
      public static var _Err_CantUseObjectHere:§j#(\x07\x02§;
      
      public static var _Err_CantTargetPet:§j#(\x07\x02§;
      
      public static var _Err_CantSwapUnavailableStudent:§j#(\x07\x02§;
      
      public static var _Err_CantSwapEmptySeats:§j#(\x07\x02§;
      
      public static var _Err_CantMoveTable:§j#(\x07\x02§;
       
      
      public var tag:String;
      
      public var index:int;
      
      public var params:Array;
      
      public const __enum__:Boolean = true;
      
      public function §j#(\x07\x02§(param1:String, param2:int, param3:*)
      {
         tag = param1;
         index = param2;
         params = param3;
      }
      
      public static function _Err_ActionUnavailable(param1:_TAction) : §j#(\x07\x02§
      {
         return new §j#(\x07\x02§("_Err_ActionUnavailable",0,[param1]);
      }
      
      public final function toString() : String
      {
         return Boot.enum_to_string(this);
      }
   }
}
