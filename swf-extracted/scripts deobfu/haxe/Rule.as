package haxe
{
   import flash.Boot;
   
   public final class Rule
   {
      
      public static const __isenum:Boolean = true;
      
      public static var __constructs__ = ["gvPy\x02","\x13\f\f\x1f\x01","u)3M\x02",";4gO\x02","Gnx\'\x03"];
      
      public static var §gvPy\x02§:Rule;
       
      
      public var tag:String;
      
      public var index:int;
      
      public var params:Array;
      
      public const __enum__:Boolean = true;
      
      public function Rule(param1:String, param2:int, param3:*)
      {
         tag = param1;
         index = param2;
         params = param3;
      }
      
      public static function §\x13\f\f\x1f\x01§(param1:String) : Rule
      {
         return new Rule("\x13\f\f\x1f\x01",1,[param1]);
      }
      
      public static function §;4gO\x02§(param1:String, param2:String) : Rule
      {
         return new Rule(";4gO\x02",3,[param1,param2]);
      }
      
      public static function §Gnx'\x03§(param1:int) : Rule
      {
         return new Rule("Gnx\'\x03",4,[param1]);
      }
      
      public static function §u)3M\x02§(param1:Rule, param2:String, param3:int) : Rule
      {
         return new Rule("u)3M\x02",2,[param1,param2,param3]);
      }
      
      public final function toString() : String
      {
         return Boot.enum_to_string(this);
      }
   }
}
