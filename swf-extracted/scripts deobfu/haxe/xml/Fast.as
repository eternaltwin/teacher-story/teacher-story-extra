package haxe.xml
{
   import flash.Boot;
   import haxe.xml._Fast.AttribAccess;
   import haxe.xml._Fast.HasAttribAccess;
   import haxe.xml._Fast.HasNodeAccess;
   import haxe.xml._Fast.NodeAccess;
   import haxe.xml._Fast.NodeListAccess;
   
   public class Fast
   {
       
      
      public var x:Xml;
      
      public var nodes:NodeListAccess;
      
      public var node:NodeAccess;
      
      public var MErd:String;
      
      public var §\x191Oo§:String;
      
      public var §')0]\x02§:String;
      
      public var hasNode:HasNodeAccess;
      
      public var has:HasAttribAccess;
      
      public var elements:Object;
      
      public var att:AttribAccess;
      
      public function Fast(param1:Xml = undefined)
      {
         if(Boot.skip_constructor)
         {
            return;
         }
         if(param1.nodeType != Xml.Document && param1.nodeType != Xml.Element)
         {
            Boot.lastError = new Error();
            throw "Invalid nodeType " + Std.string(param1.nodeType);
         }
         x = param1;
         node = new NodeAccess(param1);
         nodes = new NodeListAccess(param1);
         att = new AttribAccess(param1);
         has = new HasAttribAccess(param1);
         hasNode = new HasNodeAccess(param1);
      }
      
      public function get_name() : String
      {
         return x.nodeType == Xml.Document?"Document":x.§\x17\x1a/\x0f§();
      }
      
      public function get_innerHTML() : String
      {
         var _loc3_:* = null as Xml;
         var _loc1_:StringBuf = new StringBuf();
         var _loc2_:* = x.iterator();
         while(_loc2_.hasNext())
         {
            _loc3_ = _loc2_.next();
            _loc1_.b = _loc1_.b + Std.string(_loc3_.toString());
         }
         return _loc1_.b;
      }
      
      public function D58Y() : String
      {
         var _loc4_:* = null as Xml;
         var _loc1_:* = x.iterator();
         if(!_loc1_.hasNext())
         {
            Boot.lastError = new Error();
            throw get_name() + " does not have data";
         }
         var _loc2_:Xml = _loc1_.next();
         var _loc3_:Xml = _loc1_.next();
         if(_loc3_ != null)
         {
            if(_loc2_.nodeType == Xml.PcData && _loc3_.nodeType == Xml.CData && StringTools.trim(_loc2_.§?Yq\x01§()) == "")
            {
               _loc4_ = _loc1_.next();
               if(_loc4_ == null || _loc4_.nodeType == Xml.PcData && StringTools.trim(_loc4_.§?Yq\x01§()) == "" && _loc1_.next() == null)
               {
                  return _loc3_.§?Yq\x01§();
               }
            }
            Boot.lastError = new Error();
            throw get_name() + " does not only have data";
         }
         if(_loc2_.nodeType != Xml.PcData && _loc2_.nodeType != Xml.CData)
         {
            Boot.lastError = new Error();
            throw get_name() + " does not have data";
         }
         return _loc2_.§?Yq\x01§();
      }
      
      public function §\x06'Un\x02§() : Object
      {
         var it:Object = x.elements();
         return {
            "hasNext":it.hasNext,
            "next":function():Fast
            {
               var _loc1_:Xml = it.next();
               if(_loc1_ == null)
               {
                  return null;
               }
               return new Fast(_loc1_);
            }
         };
      }
   }
}
