package haxe.xml._Fast
{
   import flash.Boot;
   
   public dynamic class HasAttribAccess
   {
       
      
      public var __x:Xml;
      
      public function HasAttribAccess(param1:Xml = undefined)
      {
         if(Boot.skip_constructor)
         {
            return;
         }
         __x = param1;
      }
      
      public function resolve(param1:String) : Boolean
      {
         if(__x.nodeType == Xml.Document)
         {
            Boot.lastError = new Error();
            throw "Cannot access document attribute " + param1;
         }
         return Boolean(__x.exists(param1));
      }
   }
}
