package
{
   import flash.Boot;
   import flash.media.Sound;
   import flash.media.SoundLoaderContext;
   import flash.net.URLRequest;
   
   public class _SFX_projHit extends Sound
   {
       
      
      public function _SFX_projHit(param1:URLRequest = undefined, param2:SoundLoaderContext = undefined)
      {
         if(Boot.skip_constructor)
         {
            return;
         }
         super(param1,param2);
      }
   }
}
