package §=@yy§
{
   import flash.Boot;
   import flash.Lib;
   import flash.text.TextField;
   import haxe.Http;
   import haxe.Serializer;
   import haxe.Unserializer;
   import haxe.io.§*p\x1e\x02§;
   
   public class Codec
   {
      
      public static var §;\fl\x17\x02§:Boolean;
      
      public static var FAKE:Boolean = false;
      
      public static var ENCODE:String = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_";
      
      public static var §\x0ee1\n§:§\x11\x16\x1bf\x02§;
      
      public static var VERSION:String = null;
      
      public static var Types:Hash;
      
      public static var tf:TextField;
      
      public static function 'J(param1:*):void
      {
         var _loc2_:* = null as Shape;
         if(Codec.tf == null)
         {
            _loc2_ = new Shape();
            _loc2_.graphics.beginFill(16777215);
            _loc2_.graphics.drawRect(0,0,1000,1000);
            Lib.current.addChild(_loc2_);
            Codec.tf = new TextField();
            Codec.tf.width = 500;
            Codec.tf.height = 1000;
            Lib.current.addChild(Codec.tf);
            Codec.tf.selectable = true;
            Codec.tf.multiline = true;
            Codec.tf.wordWrap = true;
            Codec.tf.textColor = 16711680;
         }
         Codec.tf.text = Codec.tf.text + (Std.string(param1) + "\n");
      }
      public static var last:Http = null;
       
      
      public var §2\x01§:§*p\x1e\x02§;
      
      public var key:String;
      
      public function Codec()
      {
         var _loc4_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         if(Boot.skip_constructor)
         {
            return;
         }
         if(Codec.VERSION == null)
         {
            Codec.VERSION = "";
         }
         key = Codec.§ pXm\x01§("k");
         if(key == null)
         {
            Boot.lastError = new Error();
            throw "Missing key";
         }
         var _loc1_:String = Codec.VERSION + key;
         var _loc2_:§*p\x1e\x02§ = §*p\x1e\x02§.alloc(256);
         var _loc3_:int = 0;
         while(_loc3_ < 256)
         {
            _loc3_++;
            _loc4_ = _loc3_;
            _loc2_.b[_loc4_] = _loc4_ & 127;
         }
         _loc3_ = 0;
         _loc4_ = _loc1_.length;
         var _loc5_:int = 0;
         while(_loc5_ < 256)
         {
            _loc5_++;
            _loc6_ = _loc5_;
            _loc3_ = int(_loc3_ + int(_loc2_.b[_loc6_]) + _loc1_.charCodeAt(int(_loc6_ % _loc4_))) & 127;
            _loc7_ = int(_loc2_.b[_loc6_]);
            _loc2_.b[_loc6_] = int(_loc2_.b[_loc3_]);
            _loc2_.b[_loc3_] = _loc7_;
         }
         §2\x01§ = _loc2_;
      }
      
      public static function §XO0z\x01§(param1:String) : String
      {
         var _loc2_:String = !!Codec.FAKE?"local_sid":"sid";
         if(int(param1.indexOf("http://demo.")) == 0)
         {
            _loc2_ = "demo_sid";
         }
         return _loc2_;
      }
      
      public static function §\bpQ'\x01§(param1:String, param2:*) : void
      {
         Codec.Types.set(param1,param2);
      }
      
      public static function § pXm\x01§(param1:String) : String
      {
         return Reflect.field(Lib.current.stage.loaderInfo.parameters,param1);
      }
      
      public static function §cNp,§(param1:String, param2:*, param3:Function, param4:Object = undefined, param5:Object = undefined) : void
      {
         var url:String = param1;
         var params:* = param2;
         var §#Z_\x02§:Function = param3;
         var onError:Object = param4;
         §§push(§§newactivation());
         if(param5 == null)
         {
            param5 = 3;
         }
         var /*UnknownSlot*/:* = param5;
         var _loc6_:Http = new Http(url);
         var §h\x01§:Codec = new Codec();
         _loc6_.setParameter("__d",§h\x01§.serialize(params));
         if(onError == null)
         {
            onError = Codec.§'\x1aJ\x10§;
         }
         _loc6_.§#Z_\x02§ = function(param1:String):void
         {
            var _loc3_:* = null;
            try
            {
               _loc3_ = §h\x01§.unserialize(param1);
            }
            catch(_loc4_:*)
            {
               if(null as Error)
               {
                  Boot.lastError = null;
               }
               onError(null);
               return;
            }
            §#Z_\x02§(_loc3_);
         };
         _loc6_.onError = function(param1:String):void
         {
            if(§t\fr=§ > 0)
            {
               Codec.§cNp,§(url,params,§#Z_\x02§,onError,§t\fr=§ - 1);
            }
            else
            {
               onError(param1);
            }
         };
         _loc6_.request(true);
         Codec.last = _loc6_;
      }
      
      public static function getData(param1:String) : *
      {
         var _loc2_:String = Codec.§ pXm\x01§(param1);
         if(_loc2_ == null)
         {
            Boot.lastError = new Error();
            throw "Missing data \'" + param1 + "\'";
         }
         return new Codec().unserialize(_loc2_);
      }
      
      public static function §t\x15\x0e^§(param1:Array) : void
      {
         var _loc3_:* = null as String;
         var _loc4_:* = null as Array;
         var _loc5_:* = null as String;
         if(Codec.§\x0ee1\n§ == null)
         {
            Codec.§\x0ee1\n§ = new §\x11\x16\x1bf\x02§();
         }
         var _loc2_:int = 0;
         while(_loc2_ < int(param1.length))
         {
            _loc3_ = param1[_loc2_];
            _loc2_++;
            _loc4_ = _loc3_.split("$");
            _loc5_ = _loc4_.shift();
            Codec.§\x0ee1\n§.add(_loc4_.join("$"),_loc5_);
         }
      }
      
      public function unserialize(param1:String) : *
      {
         var _loc3_:* = null as §*p\x1e\x02§;
         var _loc4_:* = null as StringBuf;
         var _loc5_:int = 0;
         var _loc6_:int = 0;
         var _loc7_:int = 0;
         var _loc8_:int = 0;
         var _loc9_:int = 0;
         var _loc10_:* = null;
         var _loc11_:int = 0;
         var _loc12_:int = 0;
         var _loc2_:Boolean = param1.substr(0,key.length) == key;
         if(_loc2_)
         {
            Codec.FAKE = true;
         }
         if(Codec.FAKE)
         {
            if(param1.substr(0,key.length) != key)
            {
               §yf\x0b(\x03§("Invalid f-key ($v)",param1);
            }
            param1 = param1.substr(key.length);
         }
         else
         {
            _loc3_ = §2\x01§;
            _loc4_ = new StringBuf();
            _loc5_ = param1.length - 4;
            _loc6_ = int(_loc3_.b[0]);
            _loc7_ = int(_loc3_.b[1]);
            _loc8_ = 0;
            while(_loc8_ < _loc5_)
            {
               _loc8_++;
               _loc9_ = _loc8_;
               _loc10_ = param1.charCodeAt(_loc9_);
               _loc11_ = _loc10_ ^ int(_loc3_.b[_loc9_ & 255]);
               _loc12_ = _loc11_ == 0?_loc10_:_loc11_;
               _loc4_.b = _loc4_.b + String.fromCharCode(_loc12_);
               if(_loc11_ == 0)
               {
                  _loc10_ = 0;
               }
               _loc6_ = int((int(_loc6_ + _loc10_)) % 65521);
               _loc7_ = int((_loc7_ + _loc6_) % 65521);
            }
            _loc8_ = _loc6_ ^ _loc7_ << 8;
            _loc5_++;
            if(param1.charCodeAt(_loc5_) != Codec.ENCODE.charCodeAt(_loc8_ & 63) || param1.charCodeAt(_loc5_) != Codec.ENCODE.charCodeAt(_loc8_ >> 6 & 63) || param1.charCodeAt(_loc5_) != Codec.ENCODE.charCodeAt(_loc8_ >> 12 & 63) || param1.charCodeAt(_loc5_) != Codec.ENCODE.charCodeAt(_loc8_ >> 18 & 63))
            {
               §yf\x0b(\x03§("FCHK",param1);
            }
            param1 = _loc4_.b;
         }
         var _loc13_:Unserializer = new Unserializer(param1);
         _loc13_.setResolver({
            "resolveEnum":function(param1:String):*
            {
               var _loc2_:* = Codec.Types.get(param1);
               if(_loc2_ != null)
               {
                  return _loc2_;
               }
               return Type.resolveEnum(param1);
            },
            "i\x12Lx\x02":function(param1:String):*
            {
               var _loc2_:* = Codec.Types.get(param1);
               if(_loc2_ != null)
               {
                  return _loc2_;
               }
               return Type.§i\x12Lx\x02§(param1);
            }
         });
         return _loc13_.unserialize();
      }
      
      public function serializeString(param1:String) : String
      {
         var _loc8_:int = 0;
         var _loc9_:* = null;
         var _loc10_:int = 0;
         var _loc11_:int = 0;
         if(Codec.FAKE)
         {
            return key + param1;
         }
         var _loc2_:§*p\x1e\x02§ = §2\x01§;
         var _loc3_:StringBuf = new StringBuf();
         var _loc4_:int = int(_loc2_.b[0]);
         var _loc5_:int = int(_loc2_.b[1]);
         var _loc6_:int = 0;
         var _loc7_:int = param1.length;
         while(_loc6_ < _loc7_)
         {
            _loc6_++;
            _loc8_ = _loc6_;
            _loc9_ = param1.charCodeAt(_loc8_);
            _loc10_ = _loc9_ ^ int(_loc2_.b[_loc8_ & 255]);
            _loc11_ = _loc10_ == 0?_loc9_:_loc10_;
            _loc3_.b = _loc3_.b + String.fromCharCode(_loc11_);
            _loc4_ = int((_loc4_ + _loc10_) % 65521);
            _loc5_ = int((_loc5_ + _loc4_) % 65521);
         }
         _loc6_ = _loc4_ ^ _loc5_ << 8;
         _loc7_ = Codec.ENCODE.charCodeAt(_loc6_ & 63);
         _loc3_.b = _loc3_.b + String.fromCharCode(_loc7_);
         _loc7_ = Codec.ENCODE.charCodeAt(_loc6_ >> 6 & 63);
         _loc3_.b = _loc3_.b + String.fromCharCode(_loc7_);
         _loc7_ = Codec.ENCODE.charCodeAt(_loc6_ >> 12 & 63);
         _loc3_.b = _loc3_.b + String.fromCharCode(_loc7_);
         _loc7_ = Codec.ENCODE.charCodeAt(_loc6_ >> 18 & 63);
         _loc3_.b = _loc3_.b + String.fromCharCode(_loc7_);
         return _loc3_.b;
      }
      
      public function serialize(param1:*, param2:Object = undefined) : String
      {
         var _loc3_:* = null as Serializer;
         if(Codec.§\x0ee1\n§ != null)
         {
            _loc3_ = new §b\x05cK\x02§(Codec.§\x0ee1\n§);
         }
         else
         {
            _loc3_ = new Serializer();
         }
         _loc3_.useEnumIndex = true;
         if(param2)
         {
            _loc3_.serializeException(param1);
         }
         else
         {
            _loc3_.serialize(param1);
         }
         return serializeString(_loc3_.toString());
      }
      
      public function §yf\x0b(\x03§(param1:String, param2:String) : void
      {
         var _loc3_:* = null as EReg;
         if(param2.substr(0,5) == "<!DOC")
         {
            _loc3_ = new EReg("id=\"error_msg\">([^<]+)</","");
            if(_loc3_.match(param2))
            {
               Boot.lastError = new Error();
               throw StringTools.trim(_loc3_.matched(1));
            }
         }
         param2 = StringTools.trim(param2);
         if(param2.length < 100 && Boolean(new EReg("^[A-Za-z0-9 !_:-]+$","").match(param2)))
         {
            Boot.lastError = new Error();
            throw param2;
         }
         Boot.lastError = new Error();
         throw param1;
      }
   }
}
