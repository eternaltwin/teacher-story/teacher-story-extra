package mt.deepnight
{
   import flash.Boot;
   import flash.display.Sprite;
   import flash.events.Event;
   import flash.events.IOErrorEvent;
   import flash.events.ProgressEvent;
   import flash.filters.DropShadowFilter;
   import flash.filters.GlowFilter;
   import flash.media.Sound;
   import flash.media.SoundChannel;
   import flash.media.SoundTransform;
   import flash.net.URLRequest;
   import haxe.Log;
   
   public class Sfx
   {
      
      public static var §;\fl\x17\x02§:Boolean;
      
      public static var §8s3 §:Boolean = true;
      
      public static var dFMF:Array = [];
      
      public static var §\bVH\f\x01§:Boolean = false;
      
      public static var §VIH\x0b§:Boolean = false;
      
      public static var §\x1aqyV\x02§:Number = 1;
      
      public static var §=k§:Tweenie;
      
      public static var §m\x04'\x13§:IntHash;
       
      
      public var §6\x11\x03§:Number;
      
      public var §h  a§:Sound;
      
      public var §\x05nF3§:Number;
      
      public var §2dr\x0b§:Object;
      
      public var §6/\tQ\x03§:Boolean;
      
      public var §@\x1e'W\x03§:Boolean;
      
      public var §~\x01\x01\x04§:Sprite;
      
      public var §_e3h\x01§:SoundChannel;
      
      public var §$\x1c?F\x01§:int;
      
      public function Sfx(param1:Sound = undefined)
      {
         if(Boot.skip_constructor)
         {
            return;
         }
         §6\x11\x03§ = 1;
         §\x05nF3§ = 0;
         §$\x1c?F\x01§ = 0;
         §h  a§ = param1;
         §6/\tQ\x03§ = false;
         §@\x1e'W\x03§ = false;
      }
      
      public static function §\x10SQ/\x02§() : void
      {
         var _loc3_:* = null as Sfx;
         var _loc4_:* = null;
         var _loc5_:int = 0;
         var _loc1_:int = 0;
         var _loc2_:Array = Sfx.dFMF;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            if(_loc3_.§_e3h\x01§ != null)
            {
               _loc5_ = _loc3_.§$\x1c?F\x01§;
               §§push(_loc3_.§_e3h\x01§);
               §§push();
               if(Sfx.§m\x04'\x13§.exists(_loc5_))
               {
                  §§push(Sfx.§m\x04'\x13§.get(_loc5_));
               }
               else
               {
                  Sfx.§m\x04'\x13§.set(_loc5_,{
                     "6\x11\x03":Number(1),
                     "6/\tQ\x03":false
                  });
                  §§push(Sfx.§m\x04'\x13§.get(_loc5_));
               }
               _loc4_ = §§pop();
               §§pop().soundTransform = new §§pop().SoundTransform(Number(Math.max(0,Number(Math.min(1,_loc3_.§6\x11\x03§ * Sfx.§\x1aqyV\x02§ * _loc4_.§6\x11\x03§ * (!!Sfx.§VIH\x0b§?0:1) * (!!Sfx.§\bVH\f\x01§?0:1) * (!!_loc3_.§6/\tQ\x03§?0:1) * (Boolean(_loc4_.§6/\tQ\x03§)?0:1))))),_loc3_.§\x05nF3§);
            }
         }
      }
      
      public static function §G@@l§() : void
      {
         var _loc3_:* = null as Sfx;
         var _loc4_:* = null;
         var _loc5_:int = 0;
         Sfx.§\bVH\f\x01§ = true;
         var _loc1_:int = 0;
         var _loc2_:Array = Sfx.dFMF;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            if(_loc3_.§_e3h\x01§ != null)
            {
               _loc5_ = _loc3_.§$\x1c?F\x01§;
               §§push(_loc3_.§_e3h\x01§);
               §§push();
               if(Sfx.§m\x04'\x13§.exists(_loc5_))
               {
                  §§push(Sfx.§m\x04'\x13§.get(_loc5_));
               }
               else
               {
                  Sfx.§m\x04'\x13§.set(_loc5_,{
                     "6\x11\x03":Number(1),
                     "6/\tQ\x03":false
                  });
                  §§push(Sfx.§m\x04'\x13§.get(_loc5_));
               }
               _loc4_ = §§pop();
               §§pop().soundTransform = new §§pop().SoundTransform(Number(Math.max(0,Number(Math.min(1,_loc3_.§6\x11\x03§ * Sfx.§\x1aqyV\x02§ * _loc4_.§6\x11\x03§ * (!!Sfx.§VIH\x0b§?0:1) * (!!Sfx.§\bVH\f\x01§?0:1) * (!!_loc3_.§6/\tQ\x03§?0:1) * (Boolean(_loc4_.§6/\tQ\x03§)?0:1))))),_loc3_.§\x05nF3§);
            }
         }
      }
      
      public static function §zg\x11K§() : void
      {
         var _loc3_:* = null as Sfx;
         var _loc4_:* = null;
         var _loc5_:int = 0;
         Sfx.§\bVH\f\x01§ = false;
         var _loc1_:int = 0;
         var _loc2_:Array = Sfx.dFMF;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            if(_loc3_.§_e3h\x01§ != null)
            {
               _loc5_ = _loc3_.§$\x1c?F\x01§;
               §§push(_loc3_.§_e3h\x01§);
               §§push();
               if(Sfx.§m\x04'\x13§.exists(_loc5_))
               {
                  §§push(Sfx.§m\x04'\x13§.get(_loc5_));
               }
               else
               {
                  Sfx.§m\x04'\x13§.set(_loc5_,{
                     "6\x11\x03":Number(1),
                     "6/\tQ\x03":false
                  });
                  §§push(Sfx.§m\x04'\x13§.get(_loc5_));
               }
               _loc4_ = §§pop();
               §§pop().soundTransform = new §§pop().SoundTransform(Number(Math.max(0,Number(Math.min(1,_loc3_.§6\x11\x03§ * Sfx.§\x1aqyV\x02§ * _loc4_.§6\x11\x03§ * (!!Sfx.§VIH\x0b§?0:1) * (!!Sfx.§\bVH\f\x01§?0:1) * (!!_loc3_.§6/\tQ\x03§?0:1) * (Boolean(_loc4_.§6/\tQ\x03§)?0:1))))),_loc3_.§\x05nF3§);
            }
         }
      }
      
      public static function §;T\x049\x02§(param1:int, param2:Number) : void
      {
         var _loc5_:* = null as Sfx;
         var _loc6_:* = null;
         var _loc7_:int = 0;
         if(Sfx.§m\x04'\x13§.exists(param1))
         {
            §§push(Sfx.§m\x04'\x13§.get(param1));
         }
         else
         {
            Sfx.§m\x04'\x13§.set(param1,{
               "6\x11\x03":Number(1),
               "6/\tQ\x03":false
            });
            §§push(Sfx.§m\x04'\x13§.get(param1));
         }
         §§pop().§6\x11\x03§ = param2;
         var _loc3_:int = 0;
         var _loc4_:Array = Sfx.dFMF;
         while(_loc3_ < int(_loc4_.length))
         {
            _loc5_ = _loc4_[_loc3_];
            _loc3_++;
            if(_loc5_.§_e3h\x01§ != null)
            {
               _loc7_ = _loc5_.§$\x1c?F\x01§;
               §§push(_loc5_.§_e3h\x01§);
               §§push();
               if(Sfx.§m\x04'\x13§.exists(_loc7_))
               {
                  §§push(Sfx.§m\x04'\x13§.get(_loc7_));
               }
               else
               {
                  Sfx.§m\x04'\x13§.set(_loc7_,{
                     "6\x11\x03":Number(1),
                     "6/\tQ\x03":false
                  });
                  §§push(Sfx.§m\x04'\x13§.get(_loc7_));
               }
               _loc6_ = §§pop();
               §§pop().soundTransform = new §§pop().SoundTransform(Number(Math.max(0,Number(Math.min(1,_loc5_.§6\x11\x03§ * Sfx.§\x1aqyV\x02§ * _loc6_.§6\x11\x03§ * (!!Sfx.§VIH\x0b§?0:1) * (!!Sfx.§\bVH\f\x01§?0:1) * (!!_loc5_.§6/\tQ\x03§?0:1) * (Boolean(_loc6_.§6/\tQ\x03§)?0:1))))),_loc5_.§\x05nF3§);
            }
         }
      }
      
      public static function §\tm\x10}\x03§(param1:int) : void
      {
         var _loc4_:* = null as Sfx;
         var _loc5_:* = null;
         var _loc6_:int = 0;
         if(Sfx.§m\x04'\x13§.exists(param1))
         {
            §§push(Sfx.§m\x04'\x13§.get(param1));
         }
         else
         {
            Sfx.§m\x04'\x13§.set(param1,{
               "6\x11\x03":Number(1),
               "6/\tQ\x03":false
            });
            §§push(Sfx.§m\x04'\x13§.get(param1));
         }
         §§pop().§6/\tQ\x03§ = true;
         var _loc2_:int = 0;
         var _loc3_:Array = Sfx.dFMF;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            if(_loc4_.§_e3h\x01§ != null)
            {
               _loc6_ = _loc4_.§$\x1c?F\x01§;
               §§push(_loc4_.§_e3h\x01§);
               §§push();
               if(Sfx.§m\x04'\x13§.exists(_loc6_))
               {
                  §§push(Sfx.§m\x04'\x13§.get(_loc6_));
               }
               else
               {
                  Sfx.§m\x04'\x13§.set(_loc6_,{
                     "6\x11\x03":Number(1),
                     "6/\tQ\x03":false
                  });
                  §§push(Sfx.§m\x04'\x13§.get(_loc6_));
               }
               _loc5_ = §§pop();
               §§pop().soundTransform = new §§pop().SoundTransform(Number(Math.max(0,Number(Math.min(1,_loc4_.§6\x11\x03§ * Sfx.§\x1aqyV\x02§ * _loc5_.§6\x11\x03§ * (!!Sfx.§VIH\x0b§?0:1) * (!!Sfx.§\bVH\f\x01§?0:1) * (!!_loc4_.§6/\tQ\x03§?0:1) * (Boolean(_loc5_.§6/\tQ\x03§)?0:1))))),_loc4_.§\x05nF3§);
            }
         }
      }
      
      public static function §Z\x16l\x02§(param1:int) : void
      {
         var _loc4_:* = null as Sfx;
         var _loc5_:* = null;
         var _loc6_:int = 0;
         if(Sfx.§m\x04'\x13§.exists(param1))
         {
            §§push(Sfx.§m\x04'\x13§.get(param1));
         }
         else
         {
            Sfx.§m\x04'\x13§.set(param1,{
               "6\x11\x03":Number(1),
               "6/\tQ\x03":false
            });
            §§push(Sfx.§m\x04'\x13§.get(param1));
         }
         §§pop().§6/\tQ\x03§ = false;
         var _loc2_:int = 0;
         var _loc3_:Array = Sfx.dFMF;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            if(_loc4_.§_e3h\x01§ != null)
            {
               _loc6_ = _loc4_.§$\x1c?F\x01§;
               §§push(_loc4_.§_e3h\x01§);
               §§push();
               if(Sfx.§m\x04'\x13§.exists(_loc6_))
               {
                  §§push(Sfx.§m\x04'\x13§.get(_loc6_));
               }
               else
               {
                  Sfx.§m\x04'\x13§.set(_loc6_,{
                     "6\x11\x03":Number(1),
                     "6/\tQ\x03":false
                  });
                  §§push(Sfx.§m\x04'\x13§.get(_loc6_));
               }
               _loc5_ = §§pop();
               §§pop().soundTransform = new §§pop().SoundTransform(Number(Math.max(0,Number(Math.min(1,_loc4_.§6\x11\x03§ * Sfx.§\x1aqyV\x02§ * _loc5_.§6\x11\x03§ * (!!Sfx.§VIH\x0b§?0:1) * (!!Sfx.§\bVH\f\x01§?0:1) * (!!_loc4_.§6/\tQ\x03§?0:1) * (Boolean(_loc5_.§6/\tQ\x03§)?0:1))))),_loc4_.§\x05nF3§);
            }
         }
      }
      
      public static function §&`H\x01\x03§(param1:Array, param2:Object = undefined) : void
      {
         if(param2 == null)
         {
            param2 = 1;
         }
         param1[int(Std.random(int(param1.length)))]().§F\x1fyp§(param2);
      }
      
      public static function §yn\x02y\x01§(param1:Number) : Number
      {
         return Number(Math.max(0,Number(Math.min(1,param1))));
      }
      
      public static function §)LN7§(param1:Number) : Number
      {
         return Number(Math.max(-1,Number(Math.min(1,param1))));
      }
      
      public static function §md\x03\x13\x01§() : Number
      {
         return Sfx.§\x1aqyV\x02§;
      }
      
      public static function §,@/r\x02§(param1:Number) : void
      {
         var _loc4_:* = null as Sfx;
         var _loc5_:* = null;
         var _loc6_:int = 0;
         Sfx.§\x1aqyV\x02§ = Number(Math.max(0,Number(Math.min(1,param1))));
         var _loc2_:int = 0;
         var _loc3_:Array = Sfx.dFMF;
         while(_loc2_ < int(_loc3_.length))
         {
            _loc4_ = _loc3_[_loc2_];
            _loc2_++;
            if(_loc4_.§_e3h\x01§ != null)
            {
               _loc6_ = _loc4_.§$\x1c?F\x01§;
               §§push(_loc4_.§_e3h\x01§);
               §§push();
               if(Sfx.§m\x04'\x13§.exists(_loc6_))
               {
                  §§push(Sfx.§m\x04'\x13§.get(_loc6_));
               }
               else
               {
                  Sfx.§m\x04'\x13§.set(_loc6_,{
                     "6\x11\x03":Number(1),
                     "6/\tQ\x03":false
                  });
                  §§push(Sfx.§m\x04'\x13§.get(_loc6_));
               }
               _loc5_ = §§pop();
               §§pop().soundTransform = new §§pop().SoundTransform(Number(Math.max(0,Number(Math.min(1,_loc4_.§6\x11\x03§ * Sfx.§\x1aqyV\x02§ * _loc5_.§6\x11\x03§ * (!!Sfx.§VIH\x0b§?0:1) * (!!Sfx.§\bVH\f\x01§?0:1) * (!!_loc4_.§6/\tQ\x03§?0:1) * (Boolean(_loc5_.§6/\tQ\x03§)?0:1))))),_loc4_.§\x05nF3§);
            }
         }
      }
      
      public static function §\x10S\x01g\x01§() : void
      {
         var _loc3_:* = null as Sfx;
         var _loc4_:* = null;
         var _loc5_:int = 0;
         Sfx.§VIH\x0b§ = true;
         var _loc1_:int = 0;
         var _loc2_:Array = Sfx.dFMF;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            if(_loc3_.§_e3h\x01§ != null)
            {
               _loc5_ = _loc3_.§$\x1c?F\x01§;
               §§push(_loc3_.§_e3h\x01§);
               §§push();
               if(Sfx.§m\x04'\x13§.exists(_loc5_))
               {
                  §§push(Sfx.§m\x04'\x13§.get(_loc5_));
               }
               else
               {
                  Sfx.§m\x04'\x13§.set(_loc5_,{
                     "6\x11\x03":Number(1),
                     "6/\tQ\x03":false
                  });
                  §§push(Sfx.§m\x04'\x13§.get(_loc5_));
               }
               _loc4_ = §§pop();
               §§pop().soundTransform = new §§pop().SoundTransform(Number(Math.max(0,Number(Math.min(1,_loc3_.§6\x11\x03§ * Sfx.§\x1aqyV\x02§ * _loc4_.§6\x11\x03§ * (!!Sfx.§VIH\x0b§?0:1) * (!!Sfx.§\bVH\f\x01§?0:1) * (!!_loc3_.§6/\tQ\x03§?0:1) * (Boolean(_loc4_.§6/\tQ\x03§)?0:1))))),_loc3_.§\x05nF3§);
            }
         }
      }
      
      public static function Pkha() : void
      {
         var _loc3_:* = null as Sfx;
         var _loc4_:* = null;
         var _loc5_:int = 0;
         Sfx.§VIH\x0b§ = false;
         var _loc1_:int = 0;
         var _loc2_:Array = Sfx.dFMF;
         while(_loc1_ < int(_loc2_.length))
         {
            _loc3_ = _loc2_[_loc1_];
            _loc1_++;
            if(_loc3_.§_e3h\x01§ != null)
            {
               _loc5_ = _loc3_.§$\x1c?F\x01§;
               §§push(_loc3_.§_e3h\x01§);
               §§push();
               if(Sfx.§m\x04'\x13§.exists(_loc5_))
               {
                  §§push(Sfx.§m\x04'\x13§.get(_loc5_));
               }
               else
               {
                  Sfx.§m\x04'\x13§.set(_loc5_,{
                     "6\x11\x03":Number(1),
                     "6/\tQ\x03":false
                  });
                  §§push(Sfx.§m\x04'\x13§.get(_loc5_));
               }
               _loc4_ = §§pop();
               §§pop().soundTransform = new §§pop().SoundTransform(Number(Math.max(0,Number(Math.min(1,_loc3_.§6\x11\x03§ * Sfx.§\x1aqyV\x02§ * _loc4_.§6\x11\x03§ * (!!Sfx.§VIH\x0b§?0:1) * (!!Sfx.§\bVH\f\x01§?0:1) * (!!_loc3_.§6/\tQ\x03§?0:1) * (Boolean(_loc4_.§6/\tQ\x03§)?0:1))))),_loc3_.§\x05nF3§);
            }
         }
      }
      
      public static function §\x18\x02\t_\x01§(param1:int) : Object
      {
         if(Sfx.§m\x04'\x13§.exists(param1))
         {
            return Sfx.§m\x04'\x13§.get(param1);
         }
         Sfx.§m\x04'\x13§.set(param1,{
            "6\x11\x03":Number(1),
            "6/\tQ\x03":false
         });
         return Sfx.§m\x04'\x13§.get(param1);
      }
      
      public static function §\x10v,o\x02§(param1:String) : Sfx
      {
         var _loc2_:Sound = new Sound();
         var _loc3_:Sfx = new Sfx(_loc2_);
         var §3\x01§:int = flash.Lib.current.stage.stageWidth;
         var _loc4_:int = flash.Lib.current.stage.stageHeight;
         var §T)[§:Sprite = null;
         if(Sfx.§8s3 §)
         {
            §T)[§ = new Sprite();
            flash.Lib.current.addChild(§T)[§);
            §T)[§.filters = [new DropShadowFilter(2,90,16777215,0.5,0,0,1,1,true),new GlowFilter(6057869,1,2,2,10,1,true)];
            §T)[§.y = _loc4_ - 5;
         }
         var _setBar:Function = function(param1:Number):void
         {
            if(§T)[§ != null)
            {
               §T)[§.graphics.beginFill(3424080,0.5);
               §T)[§.graphics.drawRect(0,0,§3\x01§,5);
               §T)[§.graphics.beginFill(14738667,0.5);
               §T)[§.graphics.drawRect(0,0,Number(5 + (§3\x01§ - 5) * Math.max(0,Number(Math.min(1,param1)))),5);
            }
         };
         _loc2_.addEventListener(Event.COMPLETE,function(param1:*):void
         {
            if(§T)[§ != null)
            {
               §T)[§.parent.removeChild(§T)[§);
            }
         });
         _loc2_.addEventListener(ProgressEvent.PROGRESS,function(param1:ProgressEvent):void
         {
            if(§T)[§ != null)
            {
               _setBar(param1.bytesLoaded / param1.bytesTotal);
            }
         });
         _loc2_.addEventListener(IOErrorEvent.IO_ERROR,function(param1:String):void
         {
            Log.trace("Error loading SFX: " + param1,{
               "fileName":"Sfx.hx",
               "}O;\x02":346,
               "className":"mt.deepnight.Sfx",
               "methodName":"downloadAndCreate"
            });
         });
         var _loc5_:URLRequest = new URLRequest(param1);
         _loc2_.load(_loc5_);
         return _loc3_;
      }
      
      public static function §;\x01f\x1d\x03§() : void
      {
         var _loc3_:* = null;
         var _loc4_:Number = NaN;
         var _loc5_:Number = NaN;
         var _loc6_:Number = NaN;
         var _loc1_:Tweenie = Sfx.§=k§;
         var _loc2_:* = _loc1_.§FWi?§.iterator();
         while(_loc2_.hasNext())
         {
            _loc3_ = _loc2_.next();
            _loc4_ = _loc3_.§_\f§ - _loc3_.WwJP;
            if(_loc3_.§\x1bJV%§ == §\x17d/c§.§57Yw\x02§)
            {
               _loc3_.yL = Number(Number(_loc3_.yL) + (int(Std.random(100)) < 33?Number(_loc3_.§Xj(/§):Number(0)));
            }
            else
            {
               _loc3_.yL = Number(Number(_loc3_.yL) + Number(_loc3_.§Xj(/§));
            }
            _loc3_.§\x01\x01§ = Number(_loc3_.§/a\nN\x01§(Number(_loc3_.yL)));
            if(Number(_loc3_.yL) < 1)
            {
               if(_loc3_.§\x1bJV%§ != §\x17d/c§.§\x19w8\x10§ && _loc3_.§\x1bJV%§ != §\x17d/c§.§rT\x1az§)
               {
                  §§push(Number(Number(_loc3_.WwJP) + _loc3_.§\x01\x01§ * _loc4_));
               }
               else if(_loc3_.§\x1bJV%§ == §\x17d/c§.§\x19w8\x10§)
               {
                  _loc6_ = _loc3_.§\x01\x01§ * _loc4_;
                  §§push(Number(Number(_loc3_.WwJP) + Math.random() * (_loc6_ < 0?-_loc6_:_loc6_) * (_loc4_ > 0?1:-1)));
               }
               else
               {
                  §§push(Number(Number(_loc3_.WwJP) + Math.random() * _loc3_.§\x01\x01§ * _loc4_ * (int(Std.random(2)) * 2 - 1)));
               }
               _loc5_ = §§pop();
               if(_loc3_.§B\x1d\x12\x02§)
               {
                  _loc5_ = int(Math.round(_loc5_));
               }
               Reflect.§7NVD\x03§(_loc3_.parent,_loc3_.§;Xf2\x03§,_loc5_);
               if(_loc3_.§\x1d\x16nY\x02§ != null)
               {
                  _loc3_.§\x1d\x16nY\x02§();
               }
               if(_loc3_.§6|An\x03§ != null)
               {
                  _loc3_.§6|An\x03§(Number(_loc3_.yL));
               }
            }
            else
            {
               _loc1_.§-z6%\x01§(_loc3_,true);
            }
         }
      }
      
      public function §n E\x13§() : void
      {
         §6/\tQ\x03§ = false;
         §\x1aU4)\x01§(§6\x11\x03§);
      }
      
      public function §4\rXI\x03§(param1:Number, param2:§\x17d/c§ = undefined, param3:Number = undefined) : Object
      {
         var _loc4_:Number = §6\x11\x03§;
         Sfx.§=k§.§p\x1eq\x02§(this,"volume");
         §6\x11\x03§ = _loc4_;
         var _loc6_:Tweenie = Sfx.§=k§;
         var _loc7_:§\x17d/c§ = §\x17d/c§.§F\x15p§;
         var _loc5_:* = Boolean(Reflect.hasField(this,"volume"))?_loc6_.§M\rr(§(this,"volume",param1,_loc7_,param3):_loc6_.§M\rr(§(this,Boot.__unprotect__("6\x11\x03"),param1,_loc7_,param3);
         _loc5_.§\x1d\x16nY\x02§ = §V\x1aF!\x01§;
         return _loc5_;
      }
      
      public function §0\x1a\x1f$§(param1:Number, param2:§\x17d/c§ = undefined, param3:Number = undefined) : Object
      {
         var _loc4_:Number = §6\x11\x03§;
         Sfx.§=k§.§p\x1eq\x02§(this,"panning");
         §6\x11\x03§ = _loc4_;
         var _loc6_:Tweenie = Sfx.§=k§;
         var _loc7_:§\x17d/c§ = §\x17d/c§.§F\x15p§;
         var _loc5_:* = Boolean(Reflect.hasField(this,"panning"))?_loc6_.§M\rr(§(this,"panning",param1,_loc7_,param3):_loc6_.§M\rr(§(this,Boot.__unprotect__("\x05nF3"),param1,_loc7_,param3);
         _loc5_.§\x1d\x16nY\x02§ = §V\x1aF!\x01§;
         return _loc5_;
      }
      
      public function §\x18\x12e\x17\x03§() : void
      {
         §6/\tQ\x03§ = !§6/\tQ\x03§;
         §\x1aU4)\x01§(§6\x11\x03§);
      }
      
      public function toString() : String
      {
         return Std.string(§h  a§);
      }
      
      public function stop() : void
      {
         if(§_e3h\x01§ != null)
         {
            §_e3h\x01§.stop();
            §_e3h\x01§ = null;
            Sfx.dFMF.remove(this);
         }
      }
      
      public function start(param1:int, param2:Number, param3:Number, param4:Number) : void
      {
         if(Sfx.§VIH\x0b§)
         {
            return;
         }
         if(§_e3h\x01§ != null)
         {
            stop();
         }
         §6\x11\x03§ = param2;
         §\x05nF3§ = param3;
         var _loc7_:int = §$\x1c?F\x01§;
         §§push();
         if(Sfx.§m\x04'\x13§.exists(_loc7_))
         {
            §§push(Sfx.§m\x04'\x13§.get(_loc7_));
         }
         else
         {
            Sfx.§m\x04'\x13§.set(_loc7_,{
               "6\x11\x03":Number(1),
               "6/\tQ\x03":false
            });
            §§push(Sfx.§m\x04'\x13§.get(_loc7_));
         }
         var _loc6_:* = §§pop();
         var _loc5_:SoundTransform = new §§pop().SoundTransform(Number(Math.max(0,Number(Math.min(1,§6\x11\x03§ * Sfx.§\x1aqyV\x02§ * _loc6_.§6\x11\x03§ * (!!Sfx.§VIH\x0b§?0:1) * (!!Sfx.§\bVH\f\x01§?0:1) * (!!§6/\tQ\x03§?0:1) * (Boolean(_loc6_.§6/\tQ\x03§)?0:1))))),Number(Math.max(-1,Number(Math.min(1,§\x05nF3§)))));
         Sfx.dFMF.push(this);
         §_e3h\x01§ = §h  a§.play(param4,param1,_loc5_);
         §_e3h\x01§.addEventListener(Event.SOUND_COMPLETE,§\x03\x02\x0b\x01§);
      }
      
      public function §\x1aU4)\x01§(param1:Number) : void
      {
         var _loc2_:* = null;
         var _loc3_:int = 0;
         §6\x11\x03§ = param1;
         if(§_e3h\x01§ != null)
         {
            _loc3_ = §$\x1c?F\x01§;
            §§push(§_e3h\x01§);
            §§push();
            if(Sfx.§m\x04'\x13§.exists(_loc3_))
            {
               §§push(Sfx.§m\x04'\x13§.get(_loc3_));
            }
            else
            {
               Sfx.§m\x04'\x13§.set(_loc3_,{
                  "6\x11\x03":Number(1),
                  "6/\tQ\x03":false
               });
               §§push(Sfx.§m\x04'\x13§.get(_loc3_));
            }
            _loc2_ = §§pop();
            §§pop().soundTransform = new §§pop().SoundTransform(Number(Math.max(0,Number(Math.min(1,§6\x11\x03§ * Sfx.§\x1aqyV\x02§ * _loc2_.§6\x11\x03§ * (!!Sfx.§VIH\x0b§?0:1) * (!!Sfx.§\bVH\f\x01§?0:1) * (!!§6/\tQ\x03§?0:1) * (Boolean(_loc2_.§6/\tQ\x03§)?0:1))))),§\x05nF3§);
         }
      }
      
      public function §~9\x06=\x03§(param1:Number) : void
      {
         var _loc2_:* = null;
         var _loc3_:int = 0;
         §\x05nF3§ = param1;
         if(§_e3h\x01§ != null)
         {
            _loc3_ = §$\x1c?F\x01§;
            §§push(§_e3h\x01§);
            §§push();
            if(Sfx.§m\x04'\x13§.exists(_loc3_))
            {
               §§push(Sfx.§m\x04'\x13§.get(_loc3_));
            }
            else
            {
               Sfx.§m\x04'\x13§.set(_loc3_,{
                  "6\x11\x03":Number(1),
                  "6/\tQ\x03":false
               });
               §§push(Sfx.§m\x04'\x13§.get(_loc3_));
            }
            _loc2_ = §§pop();
            §§pop().soundTransform = new §§pop().SoundTransform(Number(Math.max(0,Number(Math.min(1,§6\x11\x03§ * Sfx.§\x1aqyV\x02§ * _loc2_.§6\x11\x03§ * (!!Sfx.§VIH\x0b§?0:1) * (!!Sfx.§\bVH\f\x01§?0:1) * (!!§6/\tQ\x03§?0:1) * (Boolean(_loc2_.§6/\tQ\x03§)?0:1))))),§\x05nF3§);
         }
      }
      
      public function §D -D§(param1:int) : void
      {
         var _loc2_:* = null;
         var _loc3_:int = 0;
         §$\x1c?F\x01§ = param1;
         if(§_e3h\x01§ != null)
         {
            _loc3_ = §$\x1c?F\x01§;
            §§push(§_e3h\x01§);
            §§push();
            if(Sfx.§m\x04'\x13§.exists(_loc3_))
            {
               §§push(Sfx.§m\x04'\x13§.get(_loc3_));
            }
            else
            {
               Sfx.§m\x04'\x13§.set(_loc3_,{
                  "6\x11\x03":Number(1),
                  "6/\tQ\x03":false
               });
               §§push(Sfx.§m\x04'\x13§.get(_loc3_));
            }
            _loc2_ = §§pop();
            §§pop().soundTransform = new §§pop().SoundTransform(Number(Math.max(0,Number(Math.min(1,§6\x11\x03§ * Sfx.§\x1aqyV\x02§ * _loc2_.§6\x11\x03§ * (!!Sfx.§VIH\x0b§?0:1) * (!!Sfx.§\bVH\f\x01§?0:1) * (!!§6/\tQ\x03§?0:1) * (Boolean(_loc2_.§6/\tQ\x03§)?0:1))))),§\x05nF3§);
         }
      }
      
      public function §V\x1aF!\x01§() : void
      {
         var _loc1_:* = null;
         var _loc2_:int = 0;
         if(§_e3h\x01§ != null)
         {
            _loc2_ = §$\x1c?F\x01§;
            §§push(§_e3h\x01§);
            §§push();
            if(Sfx.§m\x04'\x13§.exists(_loc2_))
            {
               §§push(Sfx.§m\x04'\x13§.get(_loc2_));
            }
            else
            {
               Sfx.§m\x04'\x13§.set(_loc2_,{
                  "6\x11\x03":Number(1),
                  "6/\tQ\x03":false
               });
               §§push(Sfx.§m\x04'\x13§.get(_loc2_));
            }
            _loc1_ = §§pop();
            §§pop().soundTransform = new §§pop().SoundTransform(Number(Math.max(0,Number(Math.min(1,§6\x11\x03§ * Sfx.§\x1aqyV\x02§ * _loc1_.§6\x11\x03§ * (!!Sfx.§VIH\x0b§?0:1) * (!!Sfx.§\bVH\f\x01§?0:1) * (!!§6/\tQ\x03§?0:1) * (Boolean(_loc1_.§6/\tQ\x03§)?0:1))))),§\x05nF3§);
         }
      }
      
      public function §&A\x03c\x02§(param1:int, param2:Object = undefined, param3:Object = undefined) : Sfx
      {
         if(param2 == null)
         {
            param2 = §6\x11\x03§;
         }
         if(param3 == null)
         {
            param3 = §\x05nF3§;
         }
         §$\x1c?F\x01§ = param1;
         start(1,param2,param3,0);
         return this;
      }
      
      public function §-[&z\x01§(param1:int, param2:Object = undefined, param3:Object = undefined, param4:Object = undefined, param5:Object = undefined) : Sfx
      {
         if(param2 == null)
         {
            param2 = 9999;
         }
         if(param5 == null)
         {
            param5 = 0;
         }
         if(param3 == null)
         {
            param3 = §6\x11\x03§;
         }
         if(param4 == null)
         {
            param4 = §\x05nF3§;
         }
         §$\x1c?F\x01§ = param1;
         start(param2,param3,param4,param5);
         return this;
      }
      
      public function §\x1aIrD\x01§(param1:Object = undefined, param2:Object = undefined, param3:Object = undefined) : Sfx
      {
         if(param1 == null)
         {
            param1 = 9999;
         }
         if(param3 == null)
         {
            param3 = 0;
         }
         if(param2 == null)
         {
            param2 = §6\x11\x03§;
         }
         start(param1,param2,0,param3);
         return this;
      }
      
      public function §F\x1fyp§(param1:Object = undefined, param2:Object = undefined) : Sfx
      {
         if(param1 == null)
         {
            param1 = §6\x11\x03§;
         }
         if(param2 == null)
         {
            param2 = §\x05nF3§;
         }
         start(1,param1,param2,0);
         return this;
      }
      
      public function §\x1b;Z\x04\x03§(param1:Function) : void
      {
         §2dr\x0b§ = param1;
      }
      
      public function §\x03\x02\x0b\x01§(param1:*) : void
      {
         var _loc2_:* = null;
         stop();
         if(§2dr\x0b§ != null)
         {
            _loc2_ = §2dr\x0b§;
            §2dr\x0b§ = null;
            _loc2_();
         }
      }
      
      public function §^vK\x18§() : void
      {
         §6/\tQ\x03§ = true;
         §\x1aU4)\x01§(§6\x11\x03§);
      }
      
      public function §J_K;§() : Boolean
      {
         return §_e3h\x01§ != null;
      }
      
      public function §\n_z{\x03§() : Number
      {
         return §h  a§.length;
      }
      
      public function §)(1T\x02§() : Number
      {
         var _loc2_:int = §$\x1c?F\x01§;
         if(Sfx.§m\x04'\x13§.exists(_loc2_))
         {
            §§push(Sfx.§m\x04'\x13§.get(_loc2_));
         }
         else
         {
            Sfx.§m\x04'\x13§.set(_loc2_,{
               "6\x11\x03":Number(1),
               "6/\tQ\x03":false
            });
            §§push(Sfx.§m\x04'\x13§.get(_loc2_));
         }
         var _loc1_:* = §§pop();
         return Number(Math.max(0,Number(Math.min(1,§6\x11\x03§ * Sfx.§\x1aqyV\x02§ * _loc1_.§6\x11\x03§ * (!!Sfx.§VIH\x0b§?0:1) * (!!Sfx.§\bVH\f\x01§?0:1) * (!!§6/\tQ\x03§?0:1) * (Boolean(_loc1_.§6/\tQ\x03§)?0:1)))));
      }
      
      public function §)e\x15\x0e§() : Number
      {
         return §\x05nF3§;
      }
   }
}
