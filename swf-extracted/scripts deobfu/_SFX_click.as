package
{
   import flash.Boot;
   import flash.media.Sound;
   import flash.media.SoundLoaderContext;
   import flash.net.URLRequest;
   
   public class _SFX_click extends Sound
   {
       
      
      public function _SFX_click(param1:URLRequest = undefined, param2:SoundLoaderContext = undefined)
      {
         if(Boot.skip_constructor)
         {
            return;
         }
         super(param1,param2);
      }
   }
}
