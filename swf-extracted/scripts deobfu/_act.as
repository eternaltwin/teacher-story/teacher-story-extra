package
{
   import flash.Boot;
   
   public final class _act
   {
      
      public static const __isenum:Boolean = true;
      
      public static var __constructs__ = ["\x13WHX\x03","\x06\x1f{Z\x03","?fZ~\x01"];
       
      
      public var tag:String;
      
      public var index:int;
      
      public var params:Array;
      
      public const __enum__:Boolean = true;
      
      public function _act(param1:String, param2:int, param3:*)
      {
         tag = param1;
         index = param2;
         params = param3;
      }
      
      public static function §?fZ~\x01§(param1:int) : _act
      {
         return new _act("?fZ~\x01",2,[param1]);
      }
      
      public static function §\x06\x1f{Z\x03§(param1:int) : _act
      {
         return new _act("\x06\x1f{Z\x03",1,[param1]);
      }
      
      public static function §\x13WHX\x03§(param1:Object) : _act
      {
         return new _act("\x13WHX\x03",0,[param1]);
      }
      
      public final function toString() : String
      {
         return Boot.enum_to_string(this);
      }
   }
}
