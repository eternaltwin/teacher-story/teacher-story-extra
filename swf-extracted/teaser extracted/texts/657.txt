[
xmin -40
ymin -40
xmax 11340
ymax 2810
wordwrap 1
multiline 1
readonly 1
noselect 1
html 1
useoutlines 1
font 656
height 300
color #fff5b232
align left
leftmargin 0
rightmargin 0
indent 0
leading 40
]<p align="left"><font face="Verdana" size="15" color="#f5b232" letterSpacing="0.000000" kerning="1"><b>Les actions importantes qui permettent une réelle gestion</b></font></p><p align="left"><font face="Verdana" size="15" color="#f5b232" letterSpacing="0.000000" kerning="1"><b>du jeu n&apos;ont pas été intégrées dans ce teaser interactif.</b></font></p><p align="left"></p><p align="left"><font face="Verdana" size="15" color="#f5b232" letterSpacing="0.000000" kerning="1"><b>Vous pouvez vous amuser à cliquer sur les éléments du décor</b></font></p><p align="left"><font face="Verdana" size="15" color="#f5b232" letterSpacing="0.000000" kerning="1"><b>et sur les élèves pour interagir !</b></font></p>