on(press){
   if(big == false || big == undefined)
   {
      this._xscale = 250;
      this._yscale = 250;
      big = true;
      _parent.mask.gotoAndPlay("on");
      _parent.barre.gotoAndPlay("on");
      _parent.mnflou.gotoAndPlay("on");
   }
   else if(big == true)
   {
      this._xscale = 200;
      this._yscale = 200;
      big = false;
      _parent.mask.gotoAndPlay("off");
      _parent.barre.gotoAndPlay("off");
      _parent.mnflou.gotoAndPlay("off");
   }
}
