function init()
{
   mcw = 590;
   mch = 320;
   fiche._visible = false;
   compt = 0;
   compt2 = 0;
   pause = false;
   arrived = false;
}
function main()
{
   compt++;
   yMask._alpha = yMask._alpha - 5;
   if(yMask._alpha <= 5)
   {
      yMask._alpha = 0;
      yMask._visible = 0;
   }
   if(compt >= random(300) + 900 && pause == false && mn1.mn2.porte._currentframe == 1 && arrived == false)
   {
      mn1.mn2.porte.gotoAndPlay("toctoc");
      mnflou.flou.mn2.porte.gotoAndPlay("toctoc");
   }
   if(compt == 50)
   {
      mn1.mn2.bubulle.gotoAndPlay("jump");
   }
   ficheLimit();
   porteOpen();
   switcher();
   projo();
   armoire();
   ficheDrag();
   eleveClick();
   dessinClick();
   rideaux();
   globe();
   if(arrived == true)
   {
      compt2++;
   }
   if(compt >= random(800) + 800 && arrived == true && mn1.mn2._currentframe >= 193 && compt2 >= 300)
   {
      gotoAndStop("tableau");
      play();
      mn1.mn2.gotoAndPlay("recre");
      gotoAndStop("tableau");
      play();
      fiche._visible = false;
   }
   ficheLimit();
   if(fiche._visible == false)
   {
      mnflou.flou._visible = false;
   }
   else
   {
      mnflou.flou._visible = true;
   }
}
function projo()
{
   mn1.mn2.projo.hit.onPress = function()
   {
      mn1.mn2.projo.play();
      mnflou.flou.mn2.projo.play();
      mn1.mn2.light.play();
      mnflou.flou.mn2.light.play();
   };
   mn1.mn2.projo.m._x = _xmouse * 1.2 - 320;
   mn1.mn2.projo.m._y = _ymouse * 1.2 - 270;
   mn1.mn2.light.m._x = _xmouse * 1.2 - 320;
   mn1.mn2.light.m._y = _ymouse * 1.2 - 270;
   mnflou.flou.mn2.projo.m._x = _xmouse * 1.2 - 320;
   mnflou.flou.mn2.projo.m._y = _ymouse * 1.2 - 270;
   mnflou.flou.mn2.light.m._x = _xmouse * 1.2 - 320;
   mnflou.flou.mn2.light.m._y = _ymouse * 1.2 - 270;
}
function porteOpen()
{
   mn1.mn2.porte.onRelease = function()
   {
      if(mn1.mn2.porte._currentframe == 1)
      {
         mn1.mn2.porte.gotoAndPlay("open");
         mnflou.flou.mn2.porte.gotoAndPlay("open");
      }
      if(mn1.mn2.porte._currentframe == 8)
      {
         mn1.mn2.porte.gotoAndPlay("close");
         mnflou.flou.mn2.porte.gotoAndPlay("close");
      }
      if(mn1.mn2.porte._currentframe >= 25)
      {
         mn1.mn2.porte.gotoAndPlay("open");
         mnflou.flou.mn2.porte.gotoAndPlay("open");
         mn1.mn2.gotoAndPlay("hikoEntre");
         mnflou.flou.mn2.gotoAndPlay("hikoEntre");
         arrived = true;
      }
   };
}
function switcher()
{
   mn1.mn2.switcher.onPress = function()
   {
      mn1.mn2.dark.play();
      mnflou.flou.mn2.dark.play();
   };
}
function armoire()
{
   mn1.mn2.armoire.onRelease = function()
   {
      if(mn1.mn2.armoire._currentframe == 1)
      {
         mn1.mn2.armoire.play();
         mnflou.flou.mn2.armoire.play();
      }
   };
}
function ficheDrag()
{
   fiche.bg.onPress = function()
   {
      fiche.startDrag();
   };
   fiche.bg.onRelease = function()
   {
      fiche.stopDrag();
      ficheLimit();
   };
   fiche.bg.onReleaseOutside = function()
   {
      fiche.stopDrag();
      ficheLimit();
   };
}
function ficheLimit()
{
   if(fiche._x <= 0)
   {
      fiche._x = 0;
   }
   if(fiche._x + fiche.bg._width * 2 * fiche._xscale / 200 >= mcw)
   {
      fiche._x = mcw - fiche.bg._width * 2 * fiche._xscale / 200;
   }
   if(fiche._y <= 0)
   {
      fiche._y = 0;
   }
   if(fiche._y + fiche.bg._height * 2 * fiche._yscale / 200 >= mch)
   {
      fiche._y = mch - fiche.bg._height * 2 * fiche._yscale / 200;
   }
}
function eleveClick()
{
   mn1.mn2.nelly.onPress = function()
   {
      fiche._visible = true;
      fiche.gotoAndStop("nelly");
      compt = compt - 100;
   };
   mn1.mn2.fatale.onPress = function()
   {
      fiche._visible = true;
      fiche.gotoAndStop("fatale");
   };
   mn1.mn2.bruce.onPress = function()
   {
      fiche._visible = true;
      fiche.gotoAndStop("bruce");
   };
   mn1.mn2.mateo.onPress = function()
   {
      fiche._visible = true;
      fiche.gotoAndStop("mateo");
   };
   mn1.mn2.maxence.onPress = function()
   {
      fiche._visible = true;
      fiche.gotoAndStop("maxence");
   };
   mn1.mn2.thibault.onPress = function()
   {
      fiche._visible = true;
      fiche.gotoAndStop("thibault");
   };
   mn1.mn2.skully.onPress = function()
   {
      fiche._visible = true;
      fiche.gotoAndStop("skully");
   };
   mn1.mn2.bubulle.onPress = function()
   {
      fiche._visible = true;
      fiche.gotoAndStop("bubulle");
   };
   mn1.mn2.camelotte.onPress = function()
   {
      fiche._visible = true;
      fiche.gotoAndStop("camelotte");
   };
   mn1.mn2.cyrielle.onPress = function()
   {
      fiche._visible = true;
      fiche.gotoAndStop("cyrielle");
   };
   mn1.mn2.macumba.onPress = function()
   {
      fiche._visible = true;
      fiche.gotoAndStop("macumba");
   };
   mn1.mn2.charlotte.onPress = function()
   {
      fiche._visible = true;
      fiche.gotoAndStop("charlotte");
   };
   mn1.mn2.nico.onPress = function()
   {
      fiche._visible = true;
      fiche.gotoAndStop("nico");
   };
   mn1.mn2.hiko.onPress = function()
   {
      fiche._visible = true;
      fiche.gotoAndStop("hiko");
   };
}
function dessinClick()
{
   mn1.mn2.licorne.onPress = function()
   {
      fiche._visible = true;
      fiche.gotoAndStop("licorne");
   };
   mn1.mn2.chardessin.onPress = function()
   {
      fiche._visible = true;
      fiche.gotoAndStop("chardessin");
   };
}
function rideaux()
{
   mn1.mn2.sol1.hit.onPress = function()
   {
      mn1.mn2.sol1.play();
      mnflou.flou.mn2.sol1.play();
   };
   mn1.mn2.sol2.hit.onPress = function()
   {
      mn1.mn2.sol2.play();
      mnflou.flou.mn2.sol2.play();
   };
   mn1.mn2.sol3.hit.onPress = function()
   {
      mn1.mn2.sol3.play();
      mnflou.flou.mn2.sol3.play();
   };
}
function globe()
{
   mn1.mn2.globe.globe.onPress = function()
   {
      mn1.mn2.globe.globe.gotoAndPlay(2);
      mnflou.flou.mn2.globe.globe.gotoAndPlay(2);
   };
}
_quality = "HIGH";
